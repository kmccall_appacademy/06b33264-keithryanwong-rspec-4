class Book
  # TODO: your code goes here!
  @@no_nos = ['the', 'a', 'an', 'and', 'in', 'of']

  def title
    @title
  end

  def title=(str)
    @title = capitalize_title(str)
  end

  def capitalize_title(str)
    title = str.split(' ').map do |word|
      @@no_nos.index(word) ? word : word.capitalize
    end
    title.join(' ')
  end
end
