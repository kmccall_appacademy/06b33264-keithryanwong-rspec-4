class Dictionary
  # TODO: your code goes here!
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    if entry.is_a? Hash
      entry.each { |key, val| @entries[key] = val }
    else
      @entries[entry] = nil
    end
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    @entries.key?(keyword)
  end

  def find(str)
    key_matches = {}
    @entries.each do |key, val|
      key_matches[key] = val if key.include? str
    end
    key_matches
  end

  def printable
    str = ''
    @entries.sort.each do |key, val|
      str.concat("\[#{key}\] \"#{val}\"\n")
    end
    str[0...-1]
  end
end
