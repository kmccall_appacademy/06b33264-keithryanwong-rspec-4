class Temperature
  # TODO: your code goes here!
  def initialize(options = {})
    if options.key?(:f)
      @deg_f, @deg_c = options[:f], to_celsius(options[:f])
    elsif options.key?(:c)
      @deg_c, @deg_f = options[:c], to_farhenheight(options[:c])
    else
      @deg_c, @deg_f = 0, 32
    end
  end

  def in_fahrenheit
    @deg_f
  end

  def in_celsius
    @deg_c
  end

  def self.from_celsius(number)
    Temperature.new(c: number)
  end

  def self.from_fahrenheit(number)
    Temperature.new(f: number)
  end

  def to_celsius(deg_f)
    ((deg_f - 32) / (9.0 / 5)).round
  end

  def to_farhenheight(deg_c)
    deg_c / (5.0 / 9) + 32
  end
end


class Celsius < Temperature
  def initialize(num)
    super({ c: num })
  end
end

class Fahrenheit < Temperature
  def initialize(num)
    super({ f: num })
  end
end
