class Timer
  def initialize
    @seconds = 0
    @time_string = time_string(0)
  end

  def seconds
    @seconds
  end

  def seconds=(time)
    @seconds = time
  end

  def time_string(seconds=self.seconds)
    time = ''
    time << padded(to_hours(seconds)) << ':'
    time << padded(to_minutes(seconds)) << ':'
    time << padded(to_seconds(seconds))
    #seconds % 60 = seconds
    #seconds / 60 % 60 = minutes
    #seconds / 3600 % 60 = minutes
  end

  def to_hours(seconds)
    (seconds / 3600) % 60
  end

  def to_minutes(seconds)
    (seconds / 60) % 60
  end

  def to_seconds(seconds)
    seconds % 60
  end

  def padded(num)
    str = num.to_s
    while str.length < 2
      str = '0' + str
    end
    str
  end
end
